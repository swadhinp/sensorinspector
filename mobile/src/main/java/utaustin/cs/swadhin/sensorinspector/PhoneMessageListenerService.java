package utaustin.cs.swadhin.sensorinspector;

import android.content.Intent;

import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.WearableListenerService;


import android.content.Intent;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import utaustin.cs.swadhin.sensorinspector.PhoneMainActivity;

public class PhoneMessageListenerService extends WearableListenerService {
    //private static final String START_ACTIVITY = "/start_activity";
    private static final String WEAR_MESSAGE_PATH = "/message";
    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if( messageEvent.getPath().equalsIgnoreCase( WEAR_MESSAGE_PATH ) ) {
          //  Intent intent = new Intent( this, PhoneMainActivity.class );
            //intent.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
            //startActivity( intent );
            String message = new String(messageEvent.getData());
            Toast.makeText(getApplicationContext(), "Time from Watch : " + message , Toast.LENGTH_LONG).show();
            long t2_watch = System.currentTimeMillis();
            sendMessage(WEAR_MESSAGE_PATH, message + ";" + String.valueOf(t2_watch));

        } else {
            super.onMessageReceived(messageEvent);
        }

    }

    private void sendMessage( final String path, final String text ) {

        NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes( PhoneMainActivity.mApiClient ).await();

        for(Node node : nodes.getNodes()) {
            MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                    PhoneMainActivity.mApiClient, node.getId(), path, text.getBytes() ).await();
        }

    }

}