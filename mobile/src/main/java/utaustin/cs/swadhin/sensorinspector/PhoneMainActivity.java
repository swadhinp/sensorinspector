package utaustin.cs.swadhin.sensorinspector;

import android.app.Activity;
import android.app.AlarmManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.io.PrintWriter;
//import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
//import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
//import android.os.PowerManager;
//import android.os.Handler;
import android.preference.PreferenceManager;
import android.content.Intent;
import android.content.pm.ActivityInfo;

import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import static java.lang.Long.parseLong;

public class PhoneMainActivity extends Activity implements MessageApi.MessageListener,GoogleApiClient.ConnectionCallbacks, SensorEventListener {

    private static final String START_ACTIVITY = "/start_activity";
    private static final String WEAR_MESSAGE_PATH = "/message";
    public static final String LOG_TAG = "SensorInspector_Debug";

    static GoogleApiClient mApiClient;
    /** Called when the activity is first created. */
    private SensorManager mSensorManager;
    private List<Sensor> deviceSensors;
    private int numSensors = 0;
    private int[] avlSensors = new int[12];
    private SimpleDateFormat dtf= new SimpleDateFormat("dd.HH.mm.ss");
    //private BufferedWriter[] fOut = new BufferedWriter[12];
    private PrintWriter[] fOut = new PrintWriter[12];
    private PrintWriter  lmFout = null;

    private Sensor mAcc;
    private Sensor mMag;
    private Sensor mGyro;
    private Sensor mProxy;
    private Sensor mLight;
    private Sensor mOrient;
    private Sensor mTemp;
    private Sensor mPressure;
    private Sensor mRotv;
    private Sensor mLacc;
    private Sensor mGravity;

    private SharedPreferences app_preferences = null;

    //sMeter = new SoundMeter("/dev/null");

    private ArrayAdapter<String> mAdapter;


    private ToggleButton record_button;
    private Button sync_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_main);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        deviceSensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);
        numSensors = deviceSensors.size();

        // Get the app's shared preferences
        app_preferences = PreferenceManager.getDefaultSharedPreferences(this);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //Blocking Auto Orientation
        int current = getRequestedOrientation();
        // only switch the orientation if not in portrait
        if (current != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        record_button = (ToggleButton) findViewById(R.id.button);
        //togButtonLoc = (ToggleButton) findViewById(R.id.locToggleButton);

        //Button
        sync_button = (Button) findViewById(R.id.button2);

        mAcc = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mMag = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mGyro = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        mProxy = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        mLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        mTemp = mSensorManager.getDefaultSensor(Sensor.TYPE_TEMPERATURE);
        mPressure = mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        mRotv = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        mLacc = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        mOrient = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        mGravity = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);

        registerListener();

        for (int i =0 ; i < numSensors; i++)
        {
            int pos = numTypeSensor(deviceSensors.get(i).getType());
            // Success! There's a sensor
            if ( pos != -1)
            {
                avlSensors[pos] = deviceSensors.get(i).getType();
            }else {
                // Failure! No sensor.
                //avlSensors[pos] = -1;····
            }
        }

        init();
        initGoogleApiClient();
    }

    public final void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Do something here if sensor accuracy changes.
    }

    public final void onSensorChanged(SensorEvent event) {

        float[] senData = null;

        senData = event.values;

        StringBuilder b = new StringBuilder();
        long tim = System.nanoTime();

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss.SSS").format(Calendar.getInstance().getTime());
        String sysTime = String.valueOf(System.currentTimeMillis());

        //Date_time:Milisecond UTC time:Nanosecond UTC time:event
        b.append(timeStamp + ":" + sysTime + ":" + Long.toString(tim));
        //b.append(Long.toString(tim)+" "+ Long.toString(event.timestamp) + " ");

        switch (event.sensor.getType()) {

            case Sensor.TYPE_ACCELEROMETER:
                b.append(appendAndShow(senData, Sensor.TYPE_ACCELEROMETER, 0));
                writeToFile(0, b);
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                b.append(appendAndShow(senData, Sensor.TYPE_MAGNETIC_FIELD, 3));
                writeToFile(1, b);
                break;
            case Sensor.TYPE_GYROSCOPE:
                b.append(appendAndShow(senData, Sensor.TYPE_GYROSCOPE, 6));
                writeToFile(2, b);
                break;
            case Sensor.TYPE_PROXIMITY:
                b.append(appendAndShow(senData, Sensor.TYPE_PROXIMITY, 9));
                writeToFile(3, b);
                break;
            case Sensor.TYPE_LIGHT:
                b.append(appendAndShow(senData, Sensor.TYPE_LIGHT, 10));
                writeToFile(4, b);
                break;
            case Sensor.TYPE_TEMPERATURE:
                b.append(appendAndShow(senData, Sensor.TYPE_TEMPERATURE, 11));
                writeToFile(5, b);
                break;
            case Sensor.TYPE_PRESSURE:
                b.append(appendAndShow(senData, Sensor.TYPE_PRESSURE, 12));
                writeToFile(6, b);
                break;
            case Sensor.TYPE_ROTATION_VECTOR:
                b.append(appendAndShow(senData, Sensor.TYPE_ROTATION_VECTOR, 13));
                writeToFile(7, b);
                //Getting Orientation From Rotation Vector
                float[] rotData = new float[16];
                //float[] rotOutData = new float[16];
                SensorManager.getRotationMatrixFromVector(rotData, senData);
                //SensorManager.remapCoordinateSystem(rotData,SensorManager.AXIS_Y,SensorManager.AXIS_MINUS_X,rotOutData);
                float[] oriData = new float[3];
                if (null != rotData)
                    SensorManager.getOrientation(rotData, oriData);

                StringBuilder c = new StringBuilder();
                c.append(timeStamp + ":" + sysTime + ":" + Long.toString(tim));
                //c.append(Long.toString(tim)+ " " + Long.toString(event.timestamp)+" ");
                c.append(appendAndShow(oriData, Sensor.TYPE_ORIENTATION, 20));
                writeToFile(10, c);
                break;

            case Sensor.TYPE_LINEAR_ACCELERATION:
                b.append(appendAndShow(senData, Sensor.TYPE_LINEAR_ACCELERATION, 16));
                writeToFile(8, b);
                break;
            //19->sound
            //writeToFile(9);
            case Sensor.TYPE_ORIENTATION: //DEPRECATED
                //b.append(appendAndShow(senData,Sensor.TYPE_ORIENTATION,20));
                //writeToFile(10,b);
                break;
            case Sensor.TYPE_GRAVITY:
                b.append(appendAndShow(senData, Sensor.TYPE_GRAVITY, 23));
                writeToFile(11, b);
                break;

            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //locationManager.requestLocationUpdates(bestProvider, 0, 0, this);
        mSensorManager.registerListener(this, mLight, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mAcc, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mMag, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mGyro, SensorManager.SENSOR_DELAY_GAME);//??
        mSensorManager.registerListener(this, mProxy, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mOrient, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mPressure, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mTemp, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mLacc, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mRotv, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mGravity, SensorManager.SENSOR_DELAY_GAME);
        //GSM
        //Tel.listen(MyListener,PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
        //wakeLock.acquire();
    }

    public void writeToFile(int i,StringBuilder b)
    {
        if( null != fOut[i] )
        {
            fOut[i].println(b.toString());
        }
    }

    public String appendAndShow(float[] senData,int type,int index)
    {
        String showData = "";
        StringBuilder bTemp = new StringBuilder();
        DecimalFormat df = new DecimalFormat("##.#####");

        if( null != senData )
        {
            for( int j =0 ; j< numOfViews(type); j++)
            {
                if( ( type == Sensor.TYPE_GYROSCOPE))
                {
                    showData = df.format(senData[j]);
                }
                else
                {
                    showData = ""+senData[j];
                }
                //bTemp.append(showData+sensorUnit(type)+",");
                bTemp.append(":" + showData );
            }
        }

        return bTemp.toString();
    }

    public int numTypeSensor(int type) {
        int result = -1;
        switch (type) {
            case Sensor.TYPE_ACCELEROMETER:
                result = 0;
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                result = 1;
                break;
            case Sensor.TYPE_GYROSCOPE:
                result = 2;
                break;
            case Sensor.TYPE_PROXIMITY:
                result = 3;
                break;
            case Sensor.TYPE_LIGHT:
                result = 4;
                break;
            case Sensor.TYPE_TEMPERATURE:
                result = 5;
                break;
            case Sensor.TYPE_PRESSURE:
                result = 6;
                break;
            case Sensor.TYPE_ROTATION_VECTOR:
                result = 7;
                break;
            case Sensor.TYPE_LINEAR_ACCELERATION:
                result = 8;
                break;
            case 99: //for sound
                result = 9;
                break;
            case Sensor.TYPE_ORIENTATION:
                result = 10;
                break;
            case Sensor.TYPE_GRAVITY:
                result = 11;
                break;
            default:
                result = -1;
                break;
        }

        return result;

    }

    public int numOfViews (int type)
    {
        int num = 0;
        switch(type)
        {
            case Sensor.TYPE_ACCELEROMETER :
            case Sensor.TYPE_MAGNETIC_FIELD :
            case Sensor.TYPE_GYROSCOPE :
            case Sensor.TYPE_ROTATION_VECTOR :
            case Sensor.TYPE_ORIENTATION :
            case Sensor.TYPE_LINEAR_ACCELERATION :
            case Sensor.TYPE_GRAVITY :
                num = 3;
                break;
            case Sensor.TYPE_LIGHT :
            case Sensor.TYPE_TEMPERATURE :
            case Sensor.TYPE_PRESSURE :
            case Sensor.TYPE_PROXIMITY :
            case 99:
                num = 1;
                break;
            default:
                num = -1;
                break;
        }

        return num;

    }

    public File fileLocation() {

        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWriteable = false;
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }
        //Log.d(LOG_TAG,Environment.getExternalStorageState()+Boolean.toString(mExternalStorageAvailable) + Boolean.toString(mExternalStorageWriteable));
        if (mExternalStorageAvailable && mExternalStorageWriteable) {

            int fNo = app_preferences.getInt("fileNo",1);
            return new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/SensorInspector/SensorInspector_"+fNo);

        }
        else {
            //No external Storage;
            return null;
        }
    }

    private File fileDir = null;
    //private PrintWriter locFout = null;

    public void registerListener() {
        if (null != record_button) {
            record_button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton cButton, boolean bVal) {
                    // Perform action on click
                    if (bVal == true) {

                        String ftag = dtf.format(new Date());
                        File[] file = new File[12];
                        //File fileDir = null;
                        //fDir=  fileDir;
                        boolean checkedVal = false;

                        fileDir = fileLocation();
                        String[] fName = new String[12];

                        for (int i = 0; i < 12; i++) {
                            fName[i] = "SensorInspector_" + ftag + "_" + i + ".txt"; //creating filenames
                        }

                        if (null != fileDir) {
                            boolean result = fileDir.mkdir();
                            Log.d(LOG_TAG, "mkdir result = " + result);
                            if (false == result) {
                                result = fileDir.mkdirs();
                            }
                            Log.d(LOG_TAG, "mkdir result = " + result);
                            if (!fileDir.getParentFile().exists() && !fileDir.getParentFile().mkdirs()) {
                                Log.d(LOG_TAG, "Unable to create " + fileDir.getParentFile());
                            }
                            Toast.makeText(getApplicationContext(), "Logging started @ " + fileLocation(), Toast.LENGTH_LONG).show();

                        } else {
                            int fNo = app_preferences.getInt("fileNo", 1);

                            fileDir = new File("/sdcard/SensorInspector/SensorInspector_" + fNo); //creating directory
                            boolean result = fileDir.mkdir();
                            Log.d(LOG_TAG, "2mkdir result = " + result);
                            if (false == result) {
                                result = fileDir.mkdirs();
                            }
                            Log.d(LOG_TAG, "2mkdir result = " + result);

                            Toast.makeText(getApplicationContext(), "Logging started @ " + "/sdcard/SensorInspector/" + fileDir, Toast.LENGTH_LONG).show();

                        }

                        for (int i = 0; i < 12; i++) {
                            file[i] = new File(fileDir, fName[i]); //opening files for storing each sensor's data
                        }

                        for (int i = 0; i < 12; i++) {
                            if (!file[i].exists()) {
                                try {
                                    file[i].createNewFile();
                                } catch (IOException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }

                            try {
                                //fOut[i] = new BufferedWriter(new FileWriter(file[i]));
                                fOut[i] = new PrintWriter(new FileWriter(file[i]));
                            } catch (IOException e) {
                                // TODO Auto-generated catch block
                                Toast.makeText(getApplicationContext(), "Logging Failure! Try Again" + e, Toast.LENGTH_SHORT).show();
                                record_button.setChecked(true);
                                e.printStackTrace();
                            }
                        }

                        //Log.d(LOG_TAG, "afterLocFileout");
                        //Writing the initial lines in each sensor data files

                        if (null != fOut[0]) {
                            fOut[0].println("#SysTm:SysTmMil:SysTmNano:Accx:Accy:Accz");
                        }

                        if (null != fOut[1]) {
                            fOut[1].println("#SysTm:SysTmMil:SysTmNano:Magx:Magy:Magz");
                        }

                        if (null != fOut[2]) {
                            fOut[2].println("#SysTm:SysTmMil:SysTmNano:Gyrox:Gyroy:Gyroz");
                        }

                        if (null != fOut[3]) {
                            fOut[3].println("#SysTm:SysTmMil:SysTmNano:Prox");
                        }

                        if (null != fOut[4]) {
                            fOut[4].println("#SysTm:SysTmMil:SysTmNano:Light");
                        }

                        if (null != fOut[5]) {
                            fOut[5].println("#SysTm:SysTmMil:SysTmNano:Temp");
                        }

                        if (null != fOut[6]) {
                            fOut[6].println("#SysTm:SysTmMil:SysTmNano:Pressure");
                        }

                        if (null != fOut[7]) {
                            fOut[7].println("#SysTm:SysTmMil:SysTmNano:Rotx:Roty:Rotz");
                        }

                        if (null != fOut[8]) {
                            fOut[8].println("#SysTm:SysTmMil:SysTmNano:Lax:Lay:Laz");
                        }

                        if (null != fOut[9]) {
                            fOut[9].println("#SysTm:SysTmMil:SysTmNano:Snd");
                        }

                        if (null != fOut[10]) {
                            fOut[10].println("#SysTm:SysTmMil:SysTmNano:Orix:Oriy:Oriz");
                        }

                        if (null != fOut[11]) {
                            fOut[11].println("#SysTm:SysTmMil:SysTmNano:Gravx:Gravy:Gravz");
                        }
                        //Log.d(LOG_TAG, "afterFiles");
                        //Log.d(LOG_TAG, "last");

                    } else {

                        record_button.setEnabled(false);

                        for (int i = 0; i < 12; i++) {
                            if (null != fOut[i]) {
                                fOut[i].close();
                            }
                        }

                        SharedPreferences.Editor filePrefEd = app_preferences.edit();
                        int val = app_preferences.getInt("fileNo", 1) + 1;
                        filePrefEd.putInt("fileNo", val);
                        filePrefEd.commit();

                        Toast.makeText(getApplicationContext(), "Data Logging stopped", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
    }

    @Override
    protected void onPause(){
        super.onPause();
        for (int i = 0; i < numSensors; i++) {
            mSensorManager.unregisterListener(this);
        }
    }

    private void initGoogleApiClient(){
        mApiClient = new GoogleApiClient.Builder( this )
                .addApi( Wearable.API )
                .build();

        mApiClient.connect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mApiClient.disconnect();
    }

    private void init() {

        sync_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(getApplicationContext(), "Start Sync From Watch" , Toast.LENGTH_LONG).show();
                //sendMessage(WEAR_MESSAGE_PATH, "Synco");
            }
        });

    }

    private void sendMessage( final String path, final String text ) {
        new Thread( new Runnable() {
            @Override
            public void run() {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes( mApiClient ).await();
                for(Node node : nodes.getNodes()) {
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            mApiClient, node.getId(), path, text.getBytes() ).await();
                }

                runOnUiThread( new Runnable() {
                    @Override
                    public void run() {
                        sync_button.setText( "Synced" );
                    }
                });
            }
        }).start();
    }


    @Override
    public void onMessageReceived( final MessageEvent messageEvent ) {

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (messageEvent.getPath().equalsIgnoreCase(WEAR_MESSAGE_PATH)) {

                    String message = new String(messageEvent.getData());
                    Toast.makeText(getApplicationContext(), "Time from Watch : " + message , Toast.LENGTH_LONG).show();
                    long t2_watch = System.currentTimeMillis();
                    sendMessage(WEAR_MESSAGE_PATH, message + ";" + String.valueOf(t2_watch));

                    //tv.setText(new String("Time Synced"));
                }
            }
        });
    }
    @Override
    public void onConnected(Bundle bundle) {
        sendMessage(START_ACTIVITY, "");
    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}