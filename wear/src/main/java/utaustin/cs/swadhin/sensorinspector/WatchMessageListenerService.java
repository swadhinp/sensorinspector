package utaustin.cs.swadhin.sensorinspector;

/**
 * Created by swadhin on 5/21/15.
 */

import android.content.Intent;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;
import android.app.AlarmManager;

import utaustin.cs.swadhin.sensorinspector.WatchMainActivity;

import static java.lang.Long.parseLong;

public class WatchMessageListenerService extends WearableListenerService {
    //private static final String START_ACTIVITY = "/start_activity";
    private static final String WEAR_MESSAGE_PATH = "/message";

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if( messageEvent.getPath().equalsIgnoreCase( WEAR_MESSAGE_PATH ) ) {
            //Intent intent = new Intent( this, WatchMainActivity.class );
            //intent.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
            //startActivity( intent );
            String message = new String(messageEvent.getData());
            //Toast.makeText(getApplicationContext(), "Time from Phone : " + message , Toast.LENGTH_LONG).show();
            String[] parts = message.split(";");

            long t1_watch = parseLong(parts[0]);
            long r1_watch = parseLong(parts[1]);
            long r2_watch = System.currentTimeMillis();

            long RTT_2   = (r2_watch-t1_watch)/2;
            long delta = t1_watch + RTT_2  - r1_watch;

            long cur_time = r1_watch + delta;

            //Setting time not possible in user mode
           // AlarmManager am = (AlarmManager) this.getSystemService(getApplicationContext().ALARM_SERVICE);
            //am.setTime(cur_time);

            //Toast.makeText(getApplicationContext(), "Time Now : " + String.valueOf(cur_time) , Toast.LENGTH_LONG).show();

        } else {
            super.onMessageReceived(messageEvent);
        }
    }

    private void sendMessage( final String path, final String text ) {

        NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes( WatchMainActivity.mApiClient ).await();

        for(Node node : nodes.getNodes()) {
            MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                    WatchMainActivity.mApiClient, node.getId(), path, text.getBytes() ).await();
        }

    }

}