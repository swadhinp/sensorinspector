package utaustin.cs.swadhin.sensorinspector;

import android.app.Activity;
import android.app.AlarmManager;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import static java.lang.Long.parseLong;

public class WatchMainActivity extends Activity implements MessageApi.MessageListener, GoogleApiClient.ConnectionCallbacks, SensorEventListener{

    private static final String TAG = "WatchMainActivity";
    private TextView mTextView;
    private static final String WEAR_MESSAGE_PATH = "/message";

    static GoogleApiClient mApiClient;
    //private ArrayAdapter<String> mAdapter;

    //private ListView mListView;

    private SensorManager mSensorManager;
    private List<Sensor> deviceSensors;

    private static final int DELAY = SensorManager.SENSOR_DELAY_GAME;

    private TextView tv;
    private TextView syncTV;

    private Button monitorBt;

    private OutputStreamWriter accWriteStream;
    private OutputStreamWriter gyrWriteStream;

    private File path=Environment.getExternalStorageDirectory();
    private File filename;

    //Sensors
    private Sensor mAcc;
    private Sensor mGyro;

    private int numSensors = 1;

    //Synchronization
    private long t1_watch = 0;
    private long t2_watch = 0;
    private long RTT_2 = 0;
    private long r1_watch = 0;
    private long delta = 0;
    private long cur_time = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rect_activity_watch_main);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        deviceSensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);
        numSensors = deviceSensors.size();

        //Populate Graphical IDs
        syncTV=(TextView) findViewById(R.id.watchTextView);
        monitorBt = (Button) findViewById(R.id.button);

        //tv.setText(String.valueOf(numSensors));

        //final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);

        //stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            //@Override
            //public void onLayoutInflated(WatchViewStub stub) {
            //    mTextView = (TextView) stub.findViewById(R.id.watchTextView);
          //  }
        //});

      //Populating Sensor IDs
        mGyro = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        mAcc = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        //Callback Function Adding
        monitorBt.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Button b = (Button) v;
                String buttonText = b.getText().toString();
                b.setText("Stop");

                if (buttonText.equals("Stop")) {

                    try{
                        //writeStream.close();
                        accWriteStream.close();
                        accWriteStream = null;
                        gyrWriteStream.close();
                        gyrWriteStream = null;

                        setResult(RESULT_OK);
                        finish();

                    }catch(IOException e){
                        e.printStackTrace();
                    }
                }
            }

        });
        syncTV.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                t1_watch = System.currentTimeMillis();
                sendMessage(WEAR_MESSAGE_PATH, String.valueOf(t1_watch));

                TextView b = (TextView) v;
                String buttonText = b.getText().toString();
                b.setText("Time Synced");

            }

        });

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        initGoogleApiClient();

        //File Writing
        try{
            filename=new File(path,"accelerometer"+".txt");
            accWriteStream = new OutputStreamWriter(new FileOutputStream(filename));
            filename = new File(path,"gyroscope"+".txt");
            gyrWriteStream= new OutputStreamWriter(new FileOutputStream(filename));

        }catch(IOException e){
            e.printStackTrace();
        }

        //stopBt.setOnClickListener(onClickListener);
        //monitorBt.setOnClickListener(onClickListener);
        mSensorManager=(SensorManager)getSystemService(Context.SENSOR_SERVICE);
    }

    private void sendMessage( final String path, final String text ) {
        PendingResult<NodeApi.GetConnectedNodesResult> nodes = Wearable.NodeApi.getConnectedNodes(mApiClient);

        //NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes( mApiClient ).await();

        nodes.setResultCallback(new ResultCallback<NodeApi.GetConnectedNodesResult>() {
            @Override
            public void onResult(NodeApi.GetConnectedNodesResult result) {
                for (int i = 0; i < result.getNodes().size(); i++) {
                    Node node = result.getNodes().get(i);
                    PendingResult<MessageApi.SendMessageResult> messageResult = Wearable.MessageApi.sendMessage(mApiClient, node.getId(),
                            path, text.getBytes());

                }
            }
        });


    }

    private void initGoogleApiClient() {
         mApiClient = new GoogleApiClient.Builder( this )
                 .addApi( Wearable.API )
                 .addConnectionCallbacks( this )
                 .build();

                if( mApiClient != null && !( mApiClient.isConnected() || mApiClient.isConnecting() ) )
                         mApiClient.connect();
    }

    @Override
    protected void onResume() {

        super.onResume();

        if( mApiClient != null && !( mApiClient.isConnected() || mApiClient.isConnecting() ) )
                       mApiClient.connect();

        boolean accT = mSensorManager.registerListener(this, mAcc, SensorManager.SENSOR_DELAY_GAME);
        boolean gyrT = mSensorManager.registerListener(this, mGyro, SensorManager.SENSOR_DELAY_GAME);

        if (accT && gyrT) {
            if (Log.isLoggable(TAG, Log.DEBUG)) {
                Log.d(TAG, "Successfully registered for the sensor updates");
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onMessageReceived( final MessageEvent messageEvent ) {

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (messageEvent.getPath().equalsIgnoreCase(WEAR_MESSAGE_PATH)) {
                    String message = new String(messageEvent.getData());
                    //Toast.makeText(getApplicationContext(), "Time from Phone : " + message, Toast.LENGTH_LONG).show();
                    String[] parts = message.split(";");

                    t1_watch = parseLong(parts[0]);
                    r1_watch = parseLong(parts[1]);
                    t2_watch = System.currentTimeMillis();

                    RTT_2   = (t2_watch-t1_watch)/2;
                    delta = t1_watch + RTT_2  - r1_watch;

                    cur_time = r1_watch + delta;
                    //AlarmManager am = (AlarmManager) getSystemService(getApplicationContext().ALARM_SERVICE);
                    //am.setTime(cur_time);

                    //Toast.makeText(getApplicationContext(), "Diff : " + String.valueOf(delta) , Toast.LENGTH_LONG).show();

                    //tv.setText(new String(messageEvent.getData()));
                    //tv.setText(new String("Time Synced"));
                }
            }
        });
    }

    @Override
    public void onConnected(Bundle bundle) {
        Wearable.MessageApi.addListener(mApiClient, this);
    }

    @Override
    protected void onStop() {
        if ( mApiClient != null ) {
            Wearable.MessageApi.removeListener( mApiClient, this );
            if ( mApiClient.isConnected() ) {
                mApiClient.disconnect();
            }
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if( mApiClient != null )
            mApiClient.unregisterConnectionCallbacks( this );
            super.onDestroy();

            setResult(RESULT_OK);
            finish();
        }

    @Override
    protected void onPause() {
        super.onPause();

        for(int i =0 ; i< 2 ; i++)
        {
            mSensorManager.unregisterListener(this);
            if (Log.isLoggable(TAG, Log.DEBUG)) {
                Log.d(TAG, "Unregistered for sensor events");
            }
        }


    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent){
        try{

            long tim=System.nanoTime();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_HHmmss.SSS");
            Date curDate = new Date(System.currentTimeMillis());
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss.SSS").format(Calendar.getInstance().getTime());
            String sysTime  = String.valueOf(System.currentTimeMillis()+delta);
            String time= formatter.format(curDate);

            if(sensorEvent.sensor.getType()==Sensor.TYPE_ACCELEROMETER){


                if (null != accWriteStream) {
                    //Date_time:Milisecond UTC time:Nanosecond UTC time
                    accWriteStream.write(timeStamp + ":" + sysTime + ":" + String.valueOf(tim) + ":" +
                            String.valueOf(sensorEvent.values[0]) + ":" +
                            String.valueOf(sensorEvent.values[1]) + ":"
                            + String.valueOf(sensorEvent.values[2]) + "\n");
                }

            }else if(sensorEvent.sensor.getType()==Sensor.TYPE_GYROSCOPE){
                if( null != gyrWriteStream){
                    //Date_time:Milisecond UTC time:Nanosecond UTC time
                    gyrWriteStream.write(timeStamp + ":" + sysTime + ":" + String.valueOf(tim) + ":" +
                            String.valueOf(sensorEvent.values[0])+ ":"+
                            String.valueOf(sensorEvent.values[1]) + ":"
                            + String.valueOf(sensorEvent.values[2]) + "\n");

                }


            }
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
}
